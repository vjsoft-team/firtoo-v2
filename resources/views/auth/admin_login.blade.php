<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
<head>
	<title>Admin Login|Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<section class="tz-login">
		<div class="tz-regi-form">
			<h4>Sign In</h4>
			<p>It's free and always will be.</p>
			<form class="col s12" method="POST" action="{{ route('admin.login.submit') }}">
          {{ csrf_field() }}
				<div class="row">
					<div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
						  <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
              <label>Email</label>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
            <input id="password" type="password" class="form-control" name="password" required>

						<label>Password</label>
            @if ($errors->has('password'))
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12"> <i class="waves-effect waves-light btn-large full-btn waves-input-wrapper" style="">
            <input type="submit" name="submit" value="submit" class="waves-button-input"></i> </div>
				</div>
			</form>
			<p><a href="{{ route('password.request') }}">forgot password</a>
         {{-- | <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }} class="filled-in" id="filled-in-box-1"> Remember Me   --}}
       </p>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:54 GMT -->
</html>

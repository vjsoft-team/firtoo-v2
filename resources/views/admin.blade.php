@php
use App\User;
use App\Listing;
use App\Addposting;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:45:06 GMT -->
<head>
	<title>{{config('app.name')}}</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	@include('backend.includes.header')
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			   @include('backend.includes.sidebar')
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/luckylu"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="/luckylu"> Dashboard</a> </li>
						<li class="page-back"><a href="/luckylu"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>Manage Booking</h4>
						<div class="tz-2-main-com bot-sp-20">
							<div class="tz-2-main-1 tz-2-main-admin">
								<a href="/luckylu/reports/listing">
								<div class="tz-2-main-2"> <img src="images/icon/d1.png" alt=""><span>All Listings</span>
									<p>All the Lorem Ipsum generators on the</p>
									@php
										$lists = Listing::all();
									@endphp
									<h2>{{count($lists)}}</h2> </div>
								</a>
							</div>
							<div class="tz-2-main-1 tz-2-main-admin">
                @php
                  $users = User::all();
                @endphp
								<a href="/luckylu/reports/users">
									<div class="tz-2-main-2"> <img src="images/icon/d4.png" alt=""><span>Users</span>
										<p>All the Lorem Ipsum generators on the</p>
										<h2>{{count($users)}}</h2> </div>
								</a>
							</div>
							 <div class="tz-2-main-1 tz-2-main-admin">
								 @php
								 	$ads = Addposting::all();
								 @endphp
								 <a href="/luckylu/reports/classifieds">
									 <div class="tz-2-main-2"> <img src="images/icon/d3.png" alt=""><span>Ads</span>
										 <p>All the Lorem Ipsum generators on the</p>
										 <h2>{{count($ads)}}</h2> </div>
								 </a>
							</div>
						{{--	<div class="tz-2-main-1 tz-2-main-admin">
								<div class="tz-2-main-2"> <img src="images/icon/d2.png" alt=""><span>Reviews</span>
									<p>All the Lorem Ipsum generators on the</p>
									<h2>53</h2> </div>
							</div> --}}
						</div>
						{{-- <div class="split-row">
							<!--== Country Campaigns ==-->
							<div class="col-md-6">
								<div class="box-inn-sp">
									<div class="inn-title">
										<h4>Country Campaigns</h4>
										<p>Airtport Hotels The Right Way To Start A Short Break Holiday</p> <a class="dropdown-button drop-down-meta" href="#" data-activates="dropdown1"><i class="material-icons">more_vert</i></a>
										<ul id="dropdown1" class="dropdown-content">
											<li><a href="#!">Add New</a> </li>
											<li><a href="#!">Edit</a> </li>
											<li><a href="#!">Update</a> </li>
											<li class="divider"></li>
											<li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
											<li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
											<li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
										</ul>
										<!-- Dropdown Structure -->
									</div>
									<div class="tab-inn">
										<div class="table-responsive table-desi">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>Country</th>
														<th>Client</th>
														<th>Changes</th>
														<th>Budget</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><span class="txt-dark weight-500">Australia</span> </td>
														<td>Beavis</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>2.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$1478</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">Cuba</span> </td>
														<td>Felix</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>1.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$951</span> </td>
														<td> <span class="label label-danger">Closed</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">France</span> </td>
														<td>Cannibus</td>
														<td><span class="txt-danger"><i class="fa fa-angle-up" aria-hidden="true"></i><span>-8.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$632</span> </td>
														<td> <span class="label label-default">Hold</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">Norway</span> </td>
														<td>Neosoft</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>7.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$325</span> </td>
														<td> <span class="label label-default">Hold</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">South Africa</span> </td>
														<td>Hencework</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>9.43%</span></span>
														</td>
														<td> <span>$258</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
							<!--== Country Campaigns ==-->
							<div class="col-md-6">
								<div class="box-inn-sp">
									<div class="inn-title">
										<h4>City Campaigns</h4>
										<p>Airtport Hotels The Right Way To Start A Short Break Holiday</p> <a class="dropdown-button drop-down-meta" href="#" data-activates="dropdown2"><i class="material-icons">more_vert</i></a>
										<ul id="dropdown2" class="dropdown-content">
											<li><a href="#!">Add New</a> </li>
											<li><a href="#!">Edit</a> </li>
											<li><a href="#!">Update</a> </li>
											<li class="divider"></li>
											<li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
											<li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
											<li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
										</ul>
										<!-- Dropdown Structure -->
									</div>
									<div class="tab-inn">
										<div class="table-responsive table-desi">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>State</th>
														<th>Client</th>
														<th>Changes</th>
														<th>Budget</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><span class="txt-dark weight-500">California</span> </td>
														<td>Beavis</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>2.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$1478</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">Florida</span> </td>
														<td>Felix</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>1.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$951</span> </td>
														<td> <span class="label label-danger">Closed</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">Hawaii</span> </td>
														<td>Cannibus</td>
														<td><span class="txt-danger"><i class="fa fa-angle-up" aria-hidden="true"></i><span>-8.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$632</span> </td>
														<td> <span class="label label-default">Hold</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">Alaska</span> </td>
														<td>Neosoft</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>7.43%</span></span>
														</td>
														<td> <span class="txt-dark weight-500">$325</span> </td>
														<td> <span class="label label-default">Hold</span> </td>
													</tr>
													<tr>
														<td><span class="txt-dark weight-500">New Jersey</span> </td>
														<td>Hencework</td>
														<td><span class="txt-success"><i class="fa fa-angle-up" aria-hidden="true"></i><span>9.43%</span></span>
														</td>
														<td> <span>$258</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div> --}}
						{{-- <div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp">
									<div class="inn-title">
										<h4>New Listing Details</h4>
										<p>Airtport Hotels The Right Way To Start A Short Break Holiday</p> <a class="dropdown-button drop-down-meta" href="#" data-activates="dr-list"><i class="material-icons">more_vert</i></a>
										<ul id="dr-list" class="dropdown-content">
											<li><a href="#!">Add New</a> </li>
											<li><a href="#!">Edit</a> </li>
											<li><a href="#!">Update</a> </li>
											<li class="divider"></li>
											<li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
											<li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
											<li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
										</ul>
										<!-- Dropdown Structure -->
									</div>
									<div class="tab-inn">
										<div class="table-responsive table-desi">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>Listing</th>
														<th>Name</th>
														<th>Phone</th>
														<th>Exp Date</th>
														<th>Country</th>
														<th>Payment</th>
														<th>Listing Type</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><span class="list-img"><img src="images/icon/dbr1.jpg" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Property Luxury Homes</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>24 Dec 2017</td>
														<td>Australia</td>
														<td> <span class="label label-primary">Pending</span> </td>
														<td> <span class="label label-danger">Premium</span> </td>
														<td> <span class="label label-primary">Pending</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/icon/dbr2.jpg" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Effi Furniture Dealers</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 6541 8541</td>
														<td>01 Jan 2018</td>
														<td>Norway</td>
														<td> <span class="label label-success">Done</span> </td>
														<td> <span class="label label-danger">Premium Plus</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/icon/dbr3.jpg" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">NIID Job Training</span><span class="list-enq-city">Express Avenue Mall, Los Angeles</span></a> </td>
														<td>+62 6541 6528</td>
														<td>24 Apr 2017</td>
														<td>England</td>
														<td> <span class="label label-success">Done</span> </td>
														<td> <span class="label label-danger">Free</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/icon/dbr4.jpg" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Property Luxury Homes</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>24 Dec 2017</td>
														<td>Australia</td>
														<td> <span class="label label-primary">Pending</span> </td>
														<td> <span class="label label-danger">Premium</span> </td>
														<td> <span class="label label-primary">Pending</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/icon/dbr5.jpg" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Effi Furniture Dealers</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 6541 8541</td>
														<td>01 Jan 2018</td>
														<td>Norway</td>
														<td> <span class="label label-success">Done</span> </td>
														<td> <span class="label label-danger">Premium Plus</span> </td>
														<td> <span class="label label-success">Active</span> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div> --}}
						{{-- <div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp">
									<div class="inn-title">
										<h4>New Leads</h4>
										<p>Airtport Hotels The Right Way To Start A Short Break Holiday</p> <a class="dropdown-button drop-down-meta" href="#" data-activates="dr-list-ad"><i class="material-icons">more_vert</i></a>
										<ul id="dr-list-ad" class="dropdown-content">
											<li><a href="#!">Add New</a> </li>
											<li><a href="#!">Edit</a> </li>
											<li><a href="#!">Update</a> </li>
											<li class="divider"></li>
											<li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
											<li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
											<li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
										</ul>
										<!-- Dropdown Structure -->
									</div>
									<div class="tab-inn">
										<div class="table-responsive table-desi">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>User</th>
														<th>Name</th>
														<th>Phone</th>
														<th>Email</th>
														<th>Category</th>
														<th>Status</th>
														<th>View</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><span class="list-img"><img src="images/users/1.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Kimberly</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Hotel</td>
														<td> <span class="label label-primary">Un Read</span> </td>
														<td> <span class="label label-primary">View</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/2.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">	Thomas</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Education</td>
														<td> <span class="label label-success">Read</span> </td>
														<td> <span class="label label-primary">View</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/5.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Charles</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Shops</td>
														<td> <span class="label label-success">Read</span> </td>
														<td> <span class="label label-primary">View</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/3.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Brittney</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Hotel</td>
														<td> <span class="label label-primary">Un Read</span> </td>
														<td> <span class="label label-primary">View</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/4.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Paul</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Education</td>
														<td> <span class="label label-success">Read</span> </td>
														<td> <span class="label label-primary">View</span> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div> --}}
						{{-- <div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp">
									<div class="inn-title">
										<h4>New User Details</h4>
										<p>Airtport Hotels The Right Way To Start A Short Break Holiday</p> <a class="dropdown-button drop-down-meta" href="#" data-activates="dr-users"><i class="material-icons">more_vert</i></a>
										<ul id="dr-users" class="dropdown-content">
											<li><a href="#!">Add New</a> </li>
											<li><a href="#!">Edit</a> </li>
											<li><a href="#!">Update</a> </li>
											<li class="divider"></li>
											<li><a href="#!"><i class="material-icons">delete</i>Delete</a> </li>
											<li><a href="#!"><i class="material-icons">subject</i>View All</a> </li>
											<li><a href="#!"><i class="material-icons">play_for_work</i>Download</a> </li>
										</ul>
										<!-- Dropdown Structure -->
									</div>
									<div class="tab-inn">
										<div class="table-responsive table-desi">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>User</th>
														<th>Name</th>
														<th>Phone</th>
														<th>Email</th>
														<th>Country</th>
														<th>Listings</th>
														<th>Enquiry</th>
														<th>Bookings</th>
														<th>Reviews</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td><span class="list-img"><img src="images/users/1.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Marsha Hogan</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Australia</td>
														<td> <span class="label label-primary">02</span> </td>
														<td> <span class="label label-danger">12</span> </td>
														<td> <span class="label label-success">24</span> </td>
														<td> <span class="label label-info">36</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/2.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Marsha Hogan</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Australia</td>
														<td> <span class="label label-primary">02</span> </td>
														<td> <span class="label label-danger">12</span> </td>
														<td> <span class="label label-success">24</span> </td>
														<td> <span class="label label-info">36</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/3.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Marsha Hogan</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Australia</td>
														<td> <span class="label label-primary">02</span> </td>
														<td> <span class="label label-danger">12</span> </td>
														<td> <span class="label label-success">24</span> </td>
														<td> <span class="label label-info">36</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/4.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Marsha Hogan</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Australia</td>
														<td> <span class="label label-primary">02</span> </td>
														<td> <span class="label label-danger">12</span> </td>
														<td> <span class="label label-success">24</span> </td>
														<td> <span class="label label-info">36</span> </td>
													</tr>
													<tr>
														<td><span class="list-img"><img src="images/users/5.png" alt=""></span> </td>
														<td><a href="#"><span class="list-enq-name">Marsha Hogan</span><span class="list-enq-city">Illunois, United States</span></a> </td>
														<td>+01 3214 6522</td>
														<td>chadengle@dummy.com</td>
														<td>Australia</td>
														<td> <span class="label label-primary">02</span> </td>
														<td> <span class="label label-danger">12</span> </td>
														<td> <span class="label label-success">24</span> </td>
														<td> <span class="label label-info">36</span> </td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div> --}}
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:45:08 GMT -->
</html>

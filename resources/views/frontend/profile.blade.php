@php
  use App\User;
  $pageId='1';
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-my-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:52 GMT -->
<head>
	<title>Profile | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('frontend.fixedsearchbar')
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			@include('frontend.usersidebar')
			<!--CENTER SECTION-->
			<div class="tz-2">
				<div class="tz-2-com tz-2-main">
					<h4>Manage My Profile</h4>
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							<h2>Profile</h2>
						</div>
            @php
            $auth_user = Auth::user()->id;
              $users_list = User::where('id', $auth_user)->get();
            @endphp
						<table class="responsive-table bordered">
							<tbody>
								<tr>
									<td>User Name</td>
									<td>:</td>
									<td>{{$users_list->first()->name}}</td>
								</tr>

								<tr>
									<td>Email</td>
									<td>:</td>
									<td>{{$users_list->first()->email}}</td>
								</tr>
								<tr>
									<td>Phone</td>
									<td>:</td>
									<td>+{{$users_list->first()->mobile}}</td>
								</tr>
								<tr>
									<td>Address</td>
									<td>:</td>
									<td>{{$users_list->first()->address}}</td>
								</tr>
								<tr>
									<td>Date of Birth</td>
									<td>:</td>
									<td>{{$users_list->first()->dob}}</td>
								</tr>


								<tr>
									<td>Status</td>
									<td>:</td>
									<td><span class="db-done">Active</span> </td>
								</tr>
							</tbody>
						</table>
            <div class="">
              <br>
            </div>
              <div class="row">
                <div class="col-md-3">
                  <a href="/profile/{{$users_list->first()->id}}/edit" class="btn btn-info">Edit my profile</a>
                 </div>
                 <div class="col-md-3">
                   <a href="/update/{{$users_list->first()->id}}/edit" class="btn btn-success">Update Password</a> </div>
                 </div>
              </div>
					</div>
				</div>
			</div>
			<!--RIGHT SECTION-->

		</div>
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
	<section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
					<a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section>
	<!--FOOTER SECTION-->
		@include('frontend.footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="list-quo" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header dir-pop-head">
						<button type="button" class="close" data-dismiss="modal">×</button>
						<h4 class="modal-title">Get a Quotes</h4>
						<!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
					</div>
					<div class="modal-body dir-pop-body">
						<form method="post" class="form-horizontal">
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Full Name *</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="fname" placeholder="" required> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Mobile</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="mobile" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Email</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="email" placeholder=""> </div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<label class="col-md-4 control-label">Message</label>
								<div class="col-md-8 get-quo">
									<textarea class="form-control"></textarea>
								</div>
							</div>
							<!--LISTING INFORMATION-->
							<div class="form-group has-feedback ak-field">
								<div class="col-md-6 col-md-offset-4">
									<input type="submit" value="SUBMIT" class="pop-btn"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-my-profile.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:52 GMT -->
</html>

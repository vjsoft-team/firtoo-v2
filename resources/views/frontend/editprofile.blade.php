@php
	$pageId = 83;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-my-profile-edit.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:52 GMT -->
<head>
	<title>Edit Profile | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="{{config('app.url')}}/cropper/cropper.css" rel="stylesheet" type="text/css">

  {{-- <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css" /> --}}
	<link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css" rel="stylesheet">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--TOP SEARCH SECTION-->
	@include('frontend.fixedsearchbar')
	<!--DASHBOARD-->
	<section>
		<div class="tz">
			<!--LEFT SECTION-->
			@include('frontend.usersidebar')
			<!--CENTER SECTION-->
			<div class="tz-2">
				<div class="tz-2-com tz-2-main">
					<h4>Profile</h4>
					<div class="db-list-com tz-db-table">
						<div class="ds-boar-title">
							<h2>Edit Profile</h2>

						</div>
						<div class="tz2-form-pay tz2-form-com">

                  {{-- {!! Form::open(['action' => ['ProfileController@update',$key->id], 'method'=> 'POST', 'enctype' => 'multipart/form-data', 'id' => 'FileUpload']) !!} --}}
									<form class="" action="{{route('profileUpdate')}}" method="post">
										{{ csrf_field() }}
										<input type="hidden" name="id" value="{{$key->id}}">

                  <div class="row">
  									<div class="input-field col s12 m6">
  										<input type="email" name="email" class="validate" value={{$key->email}} disabled>
  										<label>Email id</label>
											@if ($errors->has('email'))
											<span class="help-block">
													<strong>{{ $errors->first('email') }}</strong>
											</span>
											@endif
  									</div>
  									<div class="input-field col s12 m6">
  										<input type="number" class="validate" value={{$key->mobile}} disabled>
  										<label>Phone</label>
  									</div>
  								</div>
								<div class="row">
									<div class="input-field col s12">
										<input type="text" name="name" class="validate" value="{{$key->name}}">
										<label>User Name</label>
										@if ($errors->has('name'))
										<span class="help-block">
												<strong>{{ $errors->first('name') }}</strong>
										</span>
										@endif
									</div>
								</div>


								{{-- <div class="row">
									<div class="input-field col s12">
										<select>
											<option value="" disabled selected>Select Status</option>
											<option value="1">Active</option>
											<option value="2">Non-Active</option>
										</select>
									</div>
								</div> --}}
								<div class="row">
									<div class="input-field col s12">
										<input id="datetimepicker4" type="text" class="validate" name="dob" data-date-format="yyyy/mm/dd" value="{{$key->dob}}">
										<label>Date Of Birth(yyyy/mm/dd)</label>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<input type="text" class="validate" name="address" value="{{$key->address}}">
										<label>Address</label>
									</div>
								</div>
								{{-- <div class="row tz-file-upload">
									<div class="file-field input-field">
										<div class="tz-up-btn"> <span>Profile Pic</span>
											<input type="hidden" name="cropped_value" id="cropped_value" value="" placeholder="hhh">
        							<input type="file" name="file" id="cropper" />
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text"> </div>
									</div>
								</div> --}}
								<div class="row">
									<div class="input-field col s12">

										<input type="submit" value="SUBMIT" class="waves-effect waves-light full-btn"> </div>
								</div>
							</form>

						</div>
						<div class="db-mak-pay-bot">
					</div>
				</div>
			</div>
			<!--RIGHT SECTION-->

		</div>
	</section>
	<!--END DASHBOARD-->
	<!--MOBILE APP-->
	{{-- <section class="web-app com-padd">
		<div class="container">
			<div class="row">
				<div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
				<div class="col-md-6 web-app-con">
					<h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
					<ul>
						<li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
						<li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
					</ul> <span>We'll send you a link, open it on your phone to download the app</span>
					<form>
						<ul>
							<li>
								<input type="text" placeholder="+01" /> </li>
							<li>
								<input type="number" placeholder="Enter mobile number" /> </li>
							<li>
								<input type="submit" value="Get App Link" /> </li>
						</ul>
					</form>
					<a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
					<a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
				</div>
			</div>
		</div>
	</section> --}}
	<!--FOOTER SECTION-->
		@include('frontend.footer')
	<!--QUOTS POPUP-->
	<section>
		<!-- GET QUOTES POPUP -->
		<div class="modal fade dir-pop-com" id="cropModel" role="dialog">
		 <div class="modal-dialog">
			 <div class="modal-content">
				 <div class="modal-header dir-pop-head">
					 <button type="button" class="close" id="close" data-dismiss="modal">×</button>
					 <h4 class="modal-title">Crop Profile Image</h4>
					 <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
				 </div>
				 <div class="modal-body">
					 <div class="row">
					 		<div class="col-md-12">
								<img width="100%"  src="" id="image_cropper">
					 		</div>
					 </div>
					 <br>
						<p class="text-center">
							<button type="button" class="btn btn-primary rotate" data-method="rotate" data-option="-30"><i class="fa fa-undo"></i></button>
							<button type="button" class="btn btn-primary rotate" data-method="rotate" data-option="30"><i class="fa fa-repeat"></i></button>
						</p>
				 </div>
				 <div class="modal-footer dir-pop-footer">
					 <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
					 <button type="button" class="btn btn-primary" id="picSave">Save changes</button>
				 </div>
			 </div>
		 </div>
	 </div>

		<!-- GET QUOTES Popup END -->
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
	<script type="text/javascript" src="{{config('app.url')}}/cropper/cropper.js"> </script>
	<script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>
	<script type="text/javascript">
            $(function () {
                $('#datetimepicker4').datetimepicker();
            });
        </script>
	<script type="text/javascript">
	$(function() {
	/////// Cropper Options set
	var cropper;
	var options = {
			aspectRatio: 1 / 1.2,
			minContainerWidth: 570,
			minContainerHeight: 350,
			minCropBoxWidth: 145,
			minCropBoxHeight: 145,
			rotatable: true,
			cropBoxResizable: false,
			crop: function(e) {
					$("#cropped_value").val(parseInt(e.detail.width) + "," + parseInt(e.detail.height) + "," + parseInt(e.detail.x) + "," + parseInt(e.detail.y) + "," + parseInt(e.detail.rotate));
			}
	};
	///// Show cropper on existing Image
	$("body").on("click", "#image_source", function() {
			var src = $("#image_source").attr("src");
			src = src.replace("/thumb", "");
			$('#image_cropper').attr('src', src);
			$('#image_edit').val("yes");
			$("#myModal").modal("show");
	});
	/// Destroy Cropper on Model Hide
	$(".modal").on("hide.bs.modal", function() {
			$(".cropper-container").remove();
			cropper.destroy();
	});
	/// Show Cropper on Model Show
	$(".modal").on("show.bs.modal", function() {
			var image = document.getElementById('image_cropper');
			cropper = new Cropper(image, options);
	});
	/// Rotate Image
	$("#close").on("click", function() {
		// alert('poi');
		$("#cropper").val('');
		$("#cropped_value").val('');
		// $("#myModal").modal("toggle");
	});
	$("body").on("click", ".rotate", function() {
			var degree = $(this).attr("data-option");
			cropper.rotate(degree);
	});
	///// Saving Image with Ajax Call
	$("#picSave").on("click", function() {
		// alert('po');
			var form_data = $('#FileUpload')[0];

			var cropped_value = $('#cropped_value').val();
			var cropFile = $('#cropper').val();
			// var data = {cropped_value: cropped_value, file: cropFile};
			// console.log(cropFile);
			$.ajax({
					url: "{{config('app.url')}}/updateProfilePhoto", // Url to which the request is send
					type: "POST",
					// dataType: "json",
					mimeType: "multipart/form-data",
					headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}, // Type of request to be send, called as method
					data: new FormData(form_data), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
					contentType: false, // The content type used when sending data to the server.
					cache: false, // To unable request pages to be cached
					processData: false, // To send DOMDocument or non processed data file it is set to false
					success: function(res) // A function to be called if request succeeds
					{
						// console.log(res);
							var imagePath = res;
							$("#profilePicDynamic").attr('src', imagePath);
							$("#cropModel").modal("toggle");
							// console.log("Image Crop with Laravel5.4 Intervention and CropperJS jQuery Success");
					}
			});
	});

	////// When user upload image
	$(document).on("change", "#cropper", function() {
			var imagecheck = $(this).data('imagecheck'),
					file = this.files[0],
					imagefile = file.type,
					_URL = window.URL || window.webkitURL;
			img = new Image();
			img.src = _URL.createObjectURL(file);
			// alert('ki');
			img.onload = function() {
					var match = ["image/jpeg", "image/png", "image/jpg"];
					if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
							alert('Please Select A valid Image File');
							return false;
					} else {
							var reader = new FileReader();
							reader.readAsDataURL(file);
							reader.onloadend = function() { // set image data as background of div
									$(document).find('#image_cropper').attr('src', "");
									$(document).find('#image_cropper').attr('src', this.result);
									$('#image_edit').val("")
									$("#cropModel").modal("toggle");
							}
					}
			}
	});
});
	</script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/db-my-profile-edit.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:52 GMT -->
</html>

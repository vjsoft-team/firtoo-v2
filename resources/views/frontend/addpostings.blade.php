@php
use App\Category;
use App\Subcategories;
use App\City;
$pageId = 2;
@endphp
<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Add Posting | Firtoo</title>
      <!-- META TAGS -->
      <meta charset="utf-8">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON(BROWSER TAB ICON) -->
      <link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
      <!-- GOOGLE FONT -->
      <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
      <!-- FONTAWESOME ICONS -->
      <link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
      <!-- ALL CSS FILES -->
      <link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
      <link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
      <link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
      <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
      <link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="{{config('app.url')}}/js/html5shiv.js"></script>
      <script src="{{config('app.url')}}/js/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div id="preloader">
         <div id="status">&nbsp;</div>
      </div>
      <!--TOP SEARCH SECTION-->
      @include('frontend.fixedsearchbar')
      <!--DASHBOARD-->
      <section>
         <div class="tz">
            <!--LEFT SECTION-->
            @include('frontend.usersidebar')
            <!--CENTER SECTION-->
            <div class="tz-2 col-md-9">
               <div class="tz-2-com tz-2-main">
                  <h4>Ads</h4>
                  <div class="db-list-com tz-db-table">
                     <div class="ds-boar-title">
                        <h2>Post your Ads</h2>
                     </div>
                     <div class="hom-cre-acc-left hom-cre-acc-right">
                        <div class="">
                           <form class="" action="{{action('AddpostingController@store')}}" method="post"enctype="multipart/form-data" autocomplete="off">
                              {{ csrf_field() }}
                              <div class="row">
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple" onchange="getSubCategories()" id="category" name="category">
                                       <option value="" disabled selected>Select Category</option>
                                       @php
                                       $categories = Category::all();
                                       @endphp
                                       @foreach ($categories as $category)
                                       <option value="{{$category->id}}"@if ($category->id == old('category'))
                                         selected
                                       @endif>{{$category->name}}</option>
                                       @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple form-control" id="subCategory" name="subCategory">
                                       <option value="" disabled>Select</option>
                                    </select>
                                    @if ($errors->has('subCategory'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('subCategory') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s6">
                                   <select class="js-example-basic-multiple" id="city" name="city">
                                      <option value="" disabled selected>Select City</option>
                                      @php
                                      $categories = City::all();
                                      @endphp
                                      @foreach ($categories as $category)
                                      <option value="{{$category->city}}"@if ($category->city == old('city'))
                                        selected
                                      @endif>{{$category->city}}</option>
                                      @endforeach
                                   </select>
                                    {{-- <input name="city" type="text" id="autocomplete-input" class="autocomplete" autocomplete="nope" placeholder="city" required> --}}
                                    @if ($errors->has('city'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6" id="city_input" style="display: none;">
                                    <input id="other_city" name="other_city" type="text" value="" class="validate">
                                    <label for="email" class="">City Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 m6">
                                    <input id="list_name" name="item" required type="text" value="{{old('item')}}" class="validate" required>
                                    <label for="list_name">Item Name</label>
                                    @if ($errors->has('item'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('item') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6">
                                    <input id="number" name="price" type="text" value="{{old('price')}}" class="validate" required>
                                    <label for="email">Price</label>
                                    @if ($errors->has('price'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 m6">
                                    <input id="list_addr" name="bought_year" type="text" class="validate" value="{{old('bought_year')}}">
                                    <label for="list_addr">Year of Buy</label>
                                    @if ($errors->has('bought_year'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('bought_year') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6">
                                    <select  name="status" required>
                                       <option value="1">Active</option>
                                       <option value="2">In-Active</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="file-field input-field col s12 m6">
                                    <div class="tz-up-btn"> <span>File</span>
                                       <input type="file" name="image">
                                    </div>
                                    <div class="file-path-wrapper db-v2-pg-inp">
                                       <input class="file-path validate"  type="text">
                                    </div>
                                    @if ($errors->has('image'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('image') }}</strong>
                                   </span>
                                   @endif
                                 </div>
                                 <div class="file-field input-field col s12 m6">
                                    <div class="tz-up-btn"> <span>File</span>
                                       <input type="file" name="image2">
                                    </div>
                                    <div class="file-path-wrapper db-v2-pg-inp">
                                       <input class="file-path validate"  type="text">
                                    </div>
                                    @if ($errors->has('image2'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('image2') }}</strong>
                                   </span>
                                   @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="file-field input-field col s12 m6">
                                    <div class="tz-up-btn"> <span>File</span>
                                       <input type="file" name="image3">
                                    </div>
                                    <div class="file-path-wrapper db-v2-pg-inp">
                                       <input class="file-path validate"  type="text">
                                    </div>
                                    @if ($errors->has('image3'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('image3') }}</strong>
                                   </span>
                                   @endif
                                 </div>
                                 <div class="input-field col s12 m6">
                                    <textarea name="description" rows="4" placeholder="Description..." required>{{old('description')}}</textarea>
                                 </div>
                                 @if ($errors->has('description'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                                @endif
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 v2-mar-top-40">
                                    <input type="submit" value="Submit Ad" class="waves-effect waves-light btn-large full-btn" />
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--RIGHT SECTION-->
         </div>
      </section>
      <!--END DASHBOARD-->
      <!--MOBILE APP-->
      {{-- <section class="web-app com-padd">
         <div class="container">
            <div class="row">
               <div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
               <div class="col-md-6 web-app-con">
                  <h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
                  <ul>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
                  </ul>
                  <span>We'll send you a link, open it on your phone to download the app</span>
                  <form>
                     <ul>
                        <li>
                           <input type="text" placeholder="+01" />
                        </li>
                        <li>
                           <input type="number" placeholder="Enter mobile number" />
                        </li>
                        <li>
                           <input type="submit" value="Get App Link" />
                        </li>
                     </ul>
                  </form>
                  <a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
                  <a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
               </div>
            </div>
         </div>
      </section> --}}
      <!--FOOTER SECTION-->
      @include('frontend.footer')
      <!--QUOTS POPUP-->
      <section>
         <!-- GET QUOTES POPUP -->
         <div class="modal fade dir-pop-com" id="list-quo" role="dialog">
            <div class="modal-dialog">
               <div class="modal-content">
                  <div class="modal-header dir-pop-head">
                     <button type="button" class="close" data-dismiss="modal">×</button>
                     <h4 class="modal-title">Get a Quotes</h4>
                     <!--<i class="fa fa-pencil dir-pop-head-icon" aria-hidden="true"></i>-->
                  </div>
                  <div class="modal-body dir-pop-body">
                     <form method="post" class="form-horizontal">
                        <!--LISTING INFORMATION-->
                        <div class="form-group has-feedback ak-field">
                           <label class="col-md-4 control-label">Full Name *</label>
                           <div class="col-md-8">
                              <input type="text" class="form-control" name="fname" placeholder="" required>
                           </div>
                        </div>
                        <!--LISTING INFORMATION-->
                        <div class="form-group has-feedback ak-field">
                           <label class="col-md-4 control-label">Mobile</label>
                           <div class="col-md-8">
                              <input type="text" class="form-control" name="mobile" placeholder="">
                           </div>
                        </div>
                        <!--LISTING INFORMATION-->
                        <div class="form-group has-feedback ak-field">
                           <label class="col-md-4 control-label">Email</label>
                           <div class="col-md-8">
                              <input type="text" class="form-control" name="email" placeholder="">
                           </div>
                        </div>
                        <!--LISTING INFORMATION-->
                        <div class="form-group has-feedback ak-field">
                           <label class="col-md-4 control-label">Message</label>
                           <div class="col-md-8 get-quo">
                              <textarea class="form-control"></textarea>
                           </div>
                        </div>
                        <!--LISTING INFORMATION-->
                        <div class="form-group has-feedback ak-field">
                           <div class="col-md-6 col-md-offset-4">
                              <input type="submit" value="SUBMIT" class="pop-btn">
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <!-- GET QUOTES Popup END -->
      </section>
      <!--SCRIPT FILES-->
      <script src="{{config('app.url')}}/js/jquery.min.js"></script>
      <script
         src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
         integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
         crossorigin="anonymous"></script>
      <script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
      <script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
      {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"> </script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <script type="text/javascript">
         function getSubCategories() {
         var categoryId = $('#category').val();
         if(categoryId) {
         						 $.ajax({
         								 url: '/categorie/ajax/'+categoryId,
         								 type: "GET",
         								 dataType: "json",
         								 success:function(data) {
         										 $('#subCategory').empty();
         									$('#subCategory').append('<option value="" disabled>Select</option>');
         										 $.each(data, function(key, value) {
         										// console.log(this);
         												 $('#subCategory').append('<option value="'+ this.id +'">'+ this.name +'</option>');
         										 });


         								 }
         						 });
         				 }else{
         						 $('select[name="city"]').empty();
         				 }
         }
      </script>
      <script type="text/javascript">
         $(document).ready(function () {

         	$('.js-example-basic-multiple').select2({
         	width: 'resolve'
         });

         // $('.selectpicker').selectpicker();

         // $.ajax({
         // 	headers: {
         // 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         // 	},
         // 	type:'get',
         // 	url:'/cityAll',
         // 	// data:'city='+'1',
         // 	success:function(data){
         // 		console.log(data);
         // 		// var el = $('#subcategory_id');
         // 		var data = {
         	//
         // 		}
         // 		// $.each(data, function () {
         // 		// 	console.log(this.name);
         // 		// 	$('<option>').val(this['id']).text(this['name']).appendTo('#subcategory_id');
         // 		//
         // 		// });
         // 		// var obj = jQuery.parseJSON(data)
         // 		// $('#castes').val(obj[0]);
         	//
         // 		// console.log(obj[1]);
         // 	}
         // });
         $('input.autocomplete').autocomplete({
         	data: {
         		@php
         			$cities = City::all();
         		@endphp
         		@foreach ($cities as $city)
         			"{{$city->city}}": null,
         		@endforeach
         	},
         	limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
         	onAutocomplete: function(val) {
         		// Callback function when value is autcompleted.
         	},
         	minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
         });
         })
      </script>
      <script type="text/javascript">
        function input(e) {
          console.log(e.value);
          if (e.value == 'others') {
            $('#city_input').show();
          } else {
            $('#city_input').hide();
          }
        }
      </script>
      <script src="{{config('app.url')}}/js/custom.js"></script>
   </body>
</html>

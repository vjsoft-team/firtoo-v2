<section id="background" class="dir1-home-head">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="dir-ho-tl">
          <ul>
            <li>
              <a href="/home"><img class="mobileLogo" src="{{config('app.url')}}/images/firtoo.png" alt=""> </a>
            </li>
          </ul>
        </div>
      </div>
      <br><br><br><br>
      @if (Auth::user())
        <div class="col-md-12 col-sm-12">
          <div class="dir-ho-tr">
            <ul>

              @php
                $name = Auth::user()->name;
              @endphp
              <li><a href="/intel">Hi {{substr($name,0,10)}}</a> </li>
              <li><a href="/alllists">All Lists</a> </li>
              <li><a href="/classifieds">All Classified</a> </li>
              {{-- <li><a href="/user/logout">Logout</a> </li> --}}
            </ul>
          </div>
        </div>
      @else
        <div class="col-md-12 col-sm-12">
          <div class="dir-ho-tr">
            <ul>
              <li><a href="/alllists">All Lists</a> </li>
              <li><a href="/classifieds">All Classified</a> </li>
              <li><a href="/register">Register</a> </li>
              {{-- <li><a href="/login">Sign In</a> </li> --}}
            </ul>
          </div>
        </div>
      @endif

    </div>
  </div>
  <div class="container dir-ho-t-sp">
    <div class="row">
      <div class="dir-hr1">
        <div class="dir-ho-t-tit">
          <h1>Connect with the right Service Experts</h1>
          <p>Find B2B & B2C businesses contact addresses, phone numbers,<br> user ratings and reviews.</p>
        </div>


          <form class="" action="{{ route('selectCity') }}" method="post">
            {{ csrf_field() }}
            {{-- <div class="input-field">
              <input name="cityhidden" type="hidden" id="select-city" class="autocomplete" autocomplete="off" required>
            </div> --}}
            @php
              $value = session()->get('city')
            @endphp
            <div class="col-md-8">
              <input name="city" type="text" id="select-city" class="autocomplete" autocomplete="off" required value="{{ $value }}" placeholder="Select City">
              {{-- <label for="select-city">  Enter city</label> --}}
            </div>
            <div class="col-md-3">
              <input type="submit" value="search" class="waves-effect waves-light tourz-sear-btn"> </div>
          </form>

      </div>
    </div>
  </div>
</section>
@include('frontend.search')

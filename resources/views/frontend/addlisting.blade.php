@php
use App\Category;
use App\City;
$pageId = 1;
@endphp
<!DOCTYPE html>
<html lang="en">

   <head>
      <title>Add New Listing | Firtoo</title>
      <!-- META TAGS -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- FAV ICON(BROWSER TAB ICON) -->
      <link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
      <!-- GOOGLE FONT -->
      <link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
      <!-- FONTAWESOME ICONS -->
      <link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
      <!-- ALL CSS FILES -->
      <link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
      <link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
      <link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
      <!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
      <link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
      {{--
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
      --}}
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="{{config('app.url')}}/js/html5shiv.js"></script>
      <script src="{{config('app.url')}}/js/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div id="preloader">
         <div id="status">&nbsp;</div>
      </div>
      <!--TOP SEARCH SECTION-->
      @include('frontend.fixedsearchbar')
      <!--DASHBOARD-->
      <section>
         <div class="tz">
            <!--LEFT SECTION-->
            @include('frontend.usersidebar')
            <!--CENTER SECTION-->
            <div class="tz-2 col-md-9">
               <div class="tz-2-com tz-2-main">
                  <h4>Submit Listings</h4>
                  <div class="db-list-com tz-db-table">
                     <div class="ds-boar-title">
                        <h2>Add New Listings</h2>
                        <p>Fill the following fields to add new listing</p>
                     </div>
                     <div class="hom-cre-acc-left hom-cre-acc-right">
                        <div class="">
                           <form class="" action="{{action('ListingController@store')}}" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input id="list_name" name="title" required type="text" value="{{old('title')}}" class="validate" required>
                                    <label for="list_name">Listing Title</label>
                                    @if ($errors->has('title'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                <label for="slug">URL SLUG</label>
                                 <div class="input-field col s12">
                                    <input id="slug" name="slug" required type="text" value="{{old('slug')}}" class="validate" required>
                                    {{-- <label for="slug">URL SLUG</label> --}}
                                    @if ($errors->has('slug'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('slug') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 m6">
                                    <input id="list_phone" name="phone" required type="number" value="{{old('phone')}}" class="validate" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"maxlength = "10" required>
                                    <label for="list_phone">Phone</label>
                                    @if ($errors->has('phone'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6">
                                    <input id="email" name="email" type="email" value="{{old('email')}}" class="validate" required>
                                    <label for="email">Email</label>
                                    @if ($errors->has('email'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 ">
                                    <input id="list_addr" name="address" type="text" value="{{old('address')}}" class="validate" required>
                                    <label for="list_addr">Address</label>
                                    @if ($errors->has('address'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple" onchange="getSubCategories()" id="category" name="category">
                                       <option value="" disabled selected>Select Category</option>
                                       @php
                                       $categories = Category::all();
                                       @endphp
                                       @foreach ($categories as $category)
                                       <option value="{{$category->id}}" @if ($category->id == old('category'))
                                         selected
                                       @endif>{{$category->name}}</option>

                                       @endforeach
                                    </select>
                                    @if ($errors->has('category'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple form-control" id="subCategory" name="subCategory">
                                       <option value="" disabled>Select</option>
                                    </select>
																		@if ($errors->has('subCategory'))
																			<span class="help-block">
																				<strong>{{ $errors->first('subCategory') }}</strong>
																			</span>
																		@endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s6">
                                   <select class="js-example-basic-multiple" id="city" name="city" onchange="input(this)">
                                      <option value="" disabled selected>Select City</option>
                                      @php
                                      $categories = City::all();
                                      @endphp
                                      @foreach ($categories as $category)
                                      <option value="{{$category->city}}" @if ($category->city == old('city'))
                                        selected
                                      @endif>{{$category->city}}</option>

                                      @endforeach
                                      <option value="others">others</option>
                                   </select>
                                    {{-- <input name="city" type="text" id="autocomplete-input" class="autocomplete" autocomplete="" placeholder="city" required> --}}

                                    @if ($errors->has('city'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6" id="city_input" style="display: none;">
                                    <input id="other_city" name="other_city" type="text" value="" class="validate">
                                    <label for="email" class="">City Name</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 m6">
                                    <select name="opening_days">
                                       <option value="" disabled selected>Opening Days</option>
                                       <option value="8" @if (old('opening_days') == 8)
                                         selected
                                       @endif>All Days</option>
                                       <option value="1" @if (old('opening_days') == 1)
                                         selected
                                       @endif>Monday</option>
                                       <option value="2" @if (old('opening_days') == 2)
                                         selected
                                       @endif>Tuesday</option>
                                       <option value="3"@if (old('opening_days') == 3)
                                         selected
                                       @endif>Wednesday</option>
                                       <option value="4" @if (old('opening_days') == 4)
                                         selected
                                       @endif>Thursday</option>
                                       <option value="5" @if (old('opening_days') == 5)
                                         selected
                                       @endif>Friday</option>
                                       <option value="6" @if (old('opening_days') == 6)
                                         selected
                                       @endif>Saturday</option>
                                       <option value="7" @if (old('opening_days') == 7)
                                         selected
                                       @endif>Sunday</option>
                                    </select>
                                    @if ($errors->has('opening_days'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('opening_days') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s12 m6">
                                    <select  name="status">
                                       <option value="1" selected>Active</option>
                                       <option value="2">In-Active</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple form-control" id="" name="open_time">
                                       <option value="" disabled selected>Open Time</option>
                                       <option value="12:00 AM"@if (old('open_time') == "12:00 AM")
                                         selected
                                       @endif>12:00 AM</option>
                                       <option value="01:00 AM" @if (old('open_time') == "01:00 AM")
                                         selected
                                       @endif>01:00 AM</option>
                                       <option value="02:00 AM"  @if (old('open_time') =="02:00 AM")
                                         selected
                                       @endif>02:00 AM</option>
                                       <option value="03:00 AM"  @if (old('open_time') == "03:00 AM")
                                         selected
                                       @endif>03:00 AM</option>
                                       <option value="04:00 AM"  @if (old('open_time') == "04:00 AM")
                                         selected
                                       @endif>04:00 AM</option>
                                       <option value="05:00 AM"  @if (old('open_time') == "05:00 AM")
                                         selected
                                       @endif>05:00 AM</option>
                                       <option value="06:00 AM"  @if (old('open_time') == "06:00 AM")
                                         selected
                                       @endif>06:00 AM</option>
                                       <option value="07:00 AM"  @if (old('open_time') == "07:00 AM")
                                         selected
                                       @endif>07:00 AM</option>
                                       <option value="08:00 AM"  @if (old('open_time') == "08:00 AM")
                                         selected
                                       @endif>08:00 AM</option>
                                       <option value="09:00 AM" @if (old('open_time') == "09:00 AM")
                                         selected
                                       @endif>09:00 AM</option>
                                       <option value="10:00 AM"  @if (old('open_time') == "10:00 AM")
                                         selected
                                       @endif>10:00 AM</option>
                                       <option value="11:00 AM"  @if (old('open_time') == "11:00 AM")
                                         selected
                                       @endif>11:00 AM</option>
                                       <option value="12:00 PM"  @if (old('open_time') == "12:00 PM")
                                         selected
                                       @endif>12:00 PM</option>
                                       <option value="01:00 PM"  @if (old('open_time') == "01:00 PM")
                                         selected
                                       @endif>01:00 PM</option>
                                       <option value="02:00 PM"  @if (old('open_time') == "02:00 PM")
                                         selected
                                       @endif>02:00 PM</option>
                                       <option value="03:00 PM"  @if (old('open_time') == "03:00 PM")
                                         selected
                                       @endif>03:00 PM</option>
                                       <option value="04:00 PM"  @if (old('open_time') == "04:00 PM")
                                         selected
                                       @endif>04:00 PM</option>
                                       <option value="05:00 PM"  @if (old('open_time') ==" 05:00 PM")
                                         selected
                                       @endif>05:00 PM</option>
                                       <option value="06:00 PM"  @if (old('open_time') == "06:00 PM")
                                         selected
                                       @endif>06:00 PM</option>
                                       <option value="07:00 PM"  @if (old('open_time') == "07:00 PM")
                                         selected
                                       @endif>07:00 PM</option>
                                       <option value="08:00 PM"  @if (old('open_time') == "08:00 PM")
                                         selected
                                       @endif>08:00 PM</option>
                                       <option value="09:00 PM"  @if (old('open_time') == "09:00 PM")
                                         selected
                                       @endif>09:00 PM</option>
                                       <option value="10:00 PM"  @if (old('open_time') == "10:00 PM")
                                         selected
                                       @endif>10:00 PM</option>
                                       <option value="11:00 PM"  @if (old('open_time') == "11:00 PM")
                                         selected
                                       @endif>11:00 PM</option>
                                    </select>
                                    @if ($errors->has('open_time'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('open_time') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                                 <div class="input-field col s6">
                                    <select class="js-example-basic-multiple form-control" id="" name="closing_time">
                                       <option value="" disabled selected>Closing Time</option>
                                       <option value="12:00 AM" @if (old('closing_time') == "12:00 AM")
                                         selected
                                       @endif>12:00 AM</option>
                                       <option value="01:00 AM" @if (old('closing_time') == "01:00 AM")
                                         selected
                                       @endif>01:00 AM</option>
                                       <option value="02:00 AM"  @if (old('closing_time') == "02:00 AM")
                                         selected
                                       @endif>02:00 AM</option>
                                       <option value="03:00 AM"  @if (old('closing_time') == "03:00 AM")
                                         selected
                                       @endif>03:00 AM</option>
                                       <option value="04:00 AM"  @if (old('closing_time') == "04:00 AM")
                                         selected
                                       @endif>04:00 AM</option>
                                       <option value="05:00 AM"  @if (old('closing_time') == "05:00 AM")
                                         selected
                                       @endif>05:00 AM</option>
                                       <option value="06:00 AM"  @if (old('closing_time') == "06:00 AM")
                                         selected
                                       @endif>06:00 AM</option>
                                       <option value="07:00 AM"  @if (old('closing_time') == "07:00 AM")
                                         selected
                                       @endif>07:00 AM</option>
                                       <option value="08:00 AM"  @if (old('closing_time') == "08:00 AM")
                                         selected
                                       @endif>08:00 AM</option>
                                       <option value="09:00 AM" @if (old('closing_time') == "09:00 AM")
                                         selected
                                       @endif>09:00 AM</option>
                                       <option value="10:00 AM"  @if (old('closing_time') == "10:00 AM")
                                         selected
                                       @endif>10:00 AM</option>
                                       <option value="11:00 AM"  @if (old('closing_time') == "11:00 AM")
                                         selected
                                       @endif>11:00 AM</option>
                                       <option value="12:00 PM"  @if (old('closing_time') == "12:00 PM")
                                         selected
                                       @endif>12:00 PM</option>
                                       <option value="01:00 PM"  @if (old('closing_time') == "01:00 PM")
                                         selected
                                       @endif>01:00 PM</option>
                                       <option value="02:00 PM"  @if (old('closing_time') == "02:00 PM")
                                         selected
                                       @endif>02:00 PM</option>
                                       <option value="03:00 PM"  @if (old('closing_time') ==" 03:00 PM")
                                         selected
                                       @endif>03:00 PM</option>
                                       <option value="04:00 PM"  @if (old('closing_time') == "04:00 PM")
                                         selected
                                       @endif>04:00 PM</option>
                                       <option value="05:00 PM"  @if (old('closing_time') == "05:00 PM")
                                         selected
                                       @endif>05:00 PM</option>
                                       <option value="06:00 PM"  @if (old('closing_time') == "06:00 PM")
                                         selected
                                       @endif>06:00 PM</option>
                                       <option value="07:00 PM"  @if (old('closing_time') == "07:00 PM")
                                         selected
                                       @endif>07:00 PM</option>
                                       <option value="08:00 PM"  @if (old('closing_time') == "08:00 PM")
                                         selected
                                       @endif>08:00 PM</option>
                                       <option value="09:00 PM"  @if (old('closing_time') == "09:00 PM")
                                         selected
                                       @endif>09:00 PM</option>
                                       <option value="10:00 PM"  @if (old('closing_time') == "10:00 PM")
                                         selected
                                       @endif>10:00 PM</option>
                                       <option value="11:00 PM"  @if (old('closing_time') == "11:00 PM")
                                         selected
                                       @endif>11:00 PM</option>
                                    </select>
                                    @if ($errors->has('closing_time'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('closing_time') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row"> </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <textarea name="description" required id="textarea1" class="materialize-textarea">{{old('description')}}</textarea>
                                    <label for="textarea1">Listing Descriptions</label>
                                    @if ($errors->has('description'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <textarea name="about" required id="textarea1" class="materialize-textarea">{{old('about')}}</textarea>
                                    <label for="textarea1">About us</label>
                                    @if ($errors->has('about'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="db-v2-list-form-inn-tit">
                                    <h5>Social Media Informations:</h5>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input type="text" name="facebook" class="validate" value="{{old('facebook')}}">
                                    <label>www.facebook.com/directory</label>
                                    @if ($errors->has('facebook'))
                                    <span class="help-block">
                                    <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                    @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input type="text" name="google" class="validate" value="{{old('google')}}">
                                    <label>www.googleplus.com/directory</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input type="text" name="twitter" class="validate" value="{{old('twitter')}}">
                                    <label>www.twitter.com/directory</label>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="db-v2-list-form-inn-tit">
                                    <h5>Google Map:</h5>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input type="text" name="google_map" class="validate" value="{{old('google_map')}}">
                                    <label>Paste your iframe code here</label>
                                 </div>
                              </div>
                              {{-- <div class="row">
                                 <div class="db-v2-list-form-inn-tit">
                                    <h5>360 Degree View:</h5>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="input-field col s12">
                                    <input type="text" name="vr_map" class="validate" value="{{old('vr_map')}}">
                                    <label>Paste your iframe code here</label>
                                 </div>
                              </div> --}}
                              <div class="row">
                                 <div class="db-v2-list-form-inn-tit">
                                    <h5>Cover Image <span class="v2-db-form-note">(image size 1350x500):<span></h5>
                                 </div>
                              </div>
                              <div class="row tz-file-upload">
                                 <div class="file-field input-field">
                                    <div class="tz-up-btn"> <span>File</span>
                                       <input type="file" name="cover">
                                    </div>
                                    <div class="file-path-wrapper db-v2-pg-inp">
                                       <input class="file-path validate"  type="text">
                                    </div>
                                    @if ($errors->has('cover'))
                                   <span class="help-block">
                                       <strong>{{ $errors->first('cover') }}</strong>
                                   </span>
                                   @endif
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="db-v2-list-form-inn-tit">
                                    <h5>Photo Gallery <span class="v2-db-form-note">(upload multiple photos note:size 750x500):<span></h5>
                                 </div>
                              </div>
                              <div class="row tz-file-upload">
                                 <div class="file-field input-field">
                                    <div class="tz-up-btn"> <span>File</span>
                                       <input type="file" multiple name="gallery[]" >
                                    </div>
                                    <div class="file-path-wrapper db-v2-pg-inp">
                                       <input class="file-path validate"  type="text">
                                    </div>
                                 </div>
                                 @if ($errors->has('gallery.*'))
                                   <span class="help-block">
                                     <strong>{{ $errors->first('gallery.*') }}</strong>
                                   </span>
                                 @endif
                              </div>
                              <div class="row">
                                 <div class="input-field col s12 v2-mar-top-40"> <input type="submit" value="Submit Listing" class="waves-effect waves-light btn-large full-btn" /> </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!--RIGHT SECTION-->
         </div>
      </section>
      <!--END DASHBOARD-->
      <!--MOBILE APP-->
      {{-- <section class="web-app com-padd">
         <div class="container">
            <div class="row">
               <div class="col-md-6 web-app-img"> <img src="{{config('app.url')}}/images/mobile.png" alt="" /> </div>
               <div class="col-md-6 web-app-con">
                  <h2>Looking for the Best Service Provider? <span>Get the App!</span></h2>
                  <ul>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Find nearby listings</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Easy service enquiry</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Listing reviews and ratings</li>
                     <li><i class="fa fa-check" aria-hidden="true"></i> Manage your listing, enquiry and reviews</li>
                  </ul>
                  <span>We'll send you a link, open it on your phone to download the app</span>
                  <form>
                     <ul>
                        <li>
                           <input type="text" placeholder="+01" />
                        </li>
                        <li>
                           <input type="number" placeholder="Enter mobile number" />
                        </li>
                        <li>
                           <input type="submit" value="Get App Link" />
                        </li>
                     </ul>
                  </form>
                  <a href="#"><img src="{{config('app.url')}}/images/android.png" alt="" /> </a>
                  <a href="#"><img src="{{config('app.url')}}/images/apple.png" alt="" /> </a>
               </div>
            </div>
         </div>
      </section> --}}
      <!--FOOTER SECTION-->
      @include('frontend.footer')

      <script src="{{config('app.url')}}/js/jquery.min.js"></script>
      <script
         src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
         integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
         crossorigin="anonymous"></script>
      <script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
      <script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
      {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"> </script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <script type="text/javascript">
         function getSubCategories() {
         	var categoryId = $('#category').val();
         	if(categoryId) {
                       $.ajax({
                           url: '/categorie/ajax/'+categoryId,
                           type: "GET",
                           dataType: "json",
                           success:function(data) {
                               $('#subCategory').empty();
         										$('#subCategory').append('<option value="" disabled>Select</option>');
                               $.each(data, function(key, value) {
         											// console.log(this);
                                   $('#subCategory').append('<option value="'+ this.id +'">'+ this.name +'</option>');
                               });


                           }
                       });
                   }else{
                       $('select[name="city"]').empty();
                   }
         }
      </script>
      <script type="text/javascript">
         $(document).ready(function () {

            $('.js-example-basic-multiple').select2({
         		width: 'resolve'
         	});

         	// $('.selectpicker').selectpicker();

         	// $.ajax({
         	// 	headers: {
         	// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         	// 	},
         	// 	type:'get',
         	// 	url:'/cityAll',
         	// 	// data:'city='+'1',
         	// 	success:function(data){
         	// 		console.log(data);
         	// 		// var el = $('#subcategory_id');
         	// 		var data = {
            //
         	// 		}
         	// 		// $.each(data, function () {
         	// 		// 	console.log(this.name);
         	// 		// 	$('<option>').val(this['id']).text(this['name']).appendTo('#subcategory_id');
         	// 		//
         	// 		// });
         	// 		// var obj = jQuery.parseJSON(data)
         	// 		// $('#castes').val(obj[0]);
            //
         	// 		// console.log(obj[1]);
         	// 	}
         	// });
         	$('input.autocomplete').autocomplete({
         		data: {
         			@php
         				$cities = City::all();
         			@endphp
         			@foreach ($cities as $city)
         				"{{$city->city}}": null,
         			@endforeach
         		},
         		limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
         		onAutocomplete: function(val) {
         			// Callback function when value is autcompleted.
         		},
         		minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
         	});
         })
      </script>
      <script type="text/javascript">
        function input(e) {
          console.log(e.value);
          if (e.value == 'others') {
            $('#city_input').show();
          } else {
            $('#city_input').hide();
          }
        }
      </script>
      <script type="text/javascript">
      $("#list_name").on('keyup', function () {
        var slug = $("#list_name").val().toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'');
        $("#slug").val(slug);
      })
      function convertToSlug(Text)
          {
            return Text
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
          }
      </script>
			<script src="{{config('app.url')}}/js/custom.js"></script>
      {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> --}}
      {{-- <script type="text/javascript">
         $(document).ready(function() {
         	$('.js-example-basic-single').select2();
         });
      </script> --}}
   </body>

</html>

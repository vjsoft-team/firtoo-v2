<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<title>Firtoo</title>

		<link rel="stylesheet" href="{{ config('app.url') }}/split/split.css">
	</head>
	<body>
		<div class="container">
  <div class="split left">
    <h1>Classifieds</h1>
    <a href="{{ config('app.url') }}/classifieds" class="button">Browse</a>
  </div>
  <div class="split right">
    <h1>Listing</h1>
    <a href="{{ config('app.url') }}/alllists" class="button">Browse</a>
  </div>
</div>

<script type="text/javascript" src="{{ config('app.url') }}/split/split.js"> </script>
	</body>
</html>

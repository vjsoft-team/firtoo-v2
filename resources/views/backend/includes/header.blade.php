<div class="container-fluid sb1">
    <div class="row">
        <!--== LOGO ==-->
        <div class="col-md-2 col-sm-3 col-xs-6 sb1-1"> <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a> <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
            <a href="/admin" class="logo"><img src="{{config('app.url')}}/images/firtoo.png" alt="" height="30px" /> </a>
        </div>
        <!--== SEARCH ==-->
        <div class="col-md-6 col-sm-6 mob-hide">
            <form class="app-search">
                <input type="text" placeholder="Search..." class="form-control"> <a href="#"><i class="fa fa-search"></i></a> </form>
        </div>

        <!--== MY ACCCOUNT ==-->
        <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
            <!-- Dropdown Trigger -->
            <a class='waves-effect dropdown-button top-user-pro ' href='#' data-activates='top-menu'><img src="{{config('app.url')}}/images/users/6.png" alt="" />My Account </a>
            <!-- Dropdown Structure -->
            <ul id='top-menu' class='dropdown-content top-menu-sty'>
                <li><a href="/profile" class="waves-effect"><i class="fa fa-cogs"></i>Profile</a> </li>

                <li class="divider"></li>

                <li><a href="{{ route('admin.logout') }}" class="ho-dr-con-last waves-effect" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
               <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
                  <i class="fa fa-sign-in" aria-hidden="true"></i>Admin Logout</a> </li>
            </ul>
        </div>
    </div>
</div>

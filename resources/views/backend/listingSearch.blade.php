@php
  use App\City;
  use App\Category;
  use App\User;
@endphp
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-all-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:36 GMT -->
<head>
	<title>All Listings | Firtoo</title>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FAV ICON(BROWSER TAB ICON) -->
	<link rel="shortcut icon" href="{{config('app.url')}}/images/fav.ico" type="image/x-icon">
	<!-- GOOGLE FONT -->
	<link href="https://fonts.googleapis.com/css?family=Poppins%7CQuicksand:500,700" rel="stylesheet">
	<!-- FONTAWESOME ICONS -->
	<link rel="stylesheet" href="{{config('app.url')}}/css/font-awesome.min.css">
	<!-- ALL CSS FILES -->
	<link href="{{config('app.url')}}/css/materialize.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/style.css" rel="stylesheet">
	<link href="{{config('app.url')}}/css/bootstrap.css" rel="stylesheet" type="text/css" />
	<!-- RESPONSIVE.CSS ONLY FOR MOBILE AND TABLET VIEWS -->
	<link href="{{config('app.url')}}/css/responsive.css" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="{{config('app.url')}}/js/html5shiv.js"></script>
	<script src="{{config('app.url')}}/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
	<div id="preloader">
		<div id="status">&nbsp;</div>
	</div>
	<!--== MAIN CONTRAINER ==-->
	@include('backend.includes.header')
	<!--== BODY CONTNAINER ==-->
	<div class="container-fluid sb2">
		<div class="row">
			@include('backend.includes.sidebar')
			<!--== BODY INNER CONTAINER ==-->
			<div class="sb2-2">
				<!--== breadcrumbs ==-->
				<div class="sb2-2-2">
					<ul>
						<li><a href="/luckylu"><i class="fa fa-home" aria-hidden="true"></i> Home</a> </li>
						<li class="active-bre"><a href="#"> All Listing</a> </li>
						<li class="page-back"><a href="/luckylu"><i class="fa fa-backward" aria-hidden="true"></i> Back</a> </li>
					</ul>
				</div>
				<div class="tz-2 tz-2-admin">
					<div class="tz-2-com tz-2-main">
						<h4>All Listing</h4>

						<!-- Dropdown Structure -->
						<div class="split-row">
							<div class="col-md-12">
								<div class="box-inn-sp ad-inn-page">
									<div class="tab-inn ad-tab-inn">
										<div class="table-responsive">
											<table class="table table-hover">
												<thead>
													<tr>
                            <th>User Name</th>
														<th>Listing</th>
														<th>Phone</th>
														<th>Email</th>
														<th>Category</th>
														<th>City</th>
														<th>Status</th>
														<th>View</th>
														<th>Edit</th>
														<th>Delete</th>
													</tr>
												</thead>
												<tbody>
                          @php
                            $stage = '';
                          @endphp
													@foreach ($list as $posts)
                            <tr>
                              @php
                                $user = User::where('id', $posts->user_id)->get();
                                $name = $user->first()->name;
                              @endphp
  														<td><span class="list-enq-name">{{substr($name,0,10)}}</span> </td>
  														<td><a href="#"><span class="list-enq-name">{{substr($posts->title,0,10)}}</span>
                                {{-- <span class="list-enq-city">{{$posts->address}}</span> --}}
                              </a>
                              </td>
  														<td>+{{$posts->phone}}</td>
  														<td> <span class="label label-success">{{$posts->email}}</span> </td>
                              @php
                                $category = Category::where('id', $posts->category_id)->get();

                              @endphp
    														<td> <span class="label label-danger">{{$category->first()->name}}</span> </td>
                                @php
                                  $city = City::where('city', $posts->city)->get();
                                @endphp
  														<td> <span class="label label-info">{{$city->first()->city}}</span> </td>
                              @if ($posts->status == 1)
                                @php
                                $stage = "Active"
                                @endphp
                              @elseif ($posts->status == 2)
                                @php
                                $stage = "In-Active"
                                @endphp
                              @elseif ($posts->status == 3)
                                @php
                                $stage = "Sold"
                                @endphp
                              @elseif ($posts->status == 4)
                                @php
                                $stage = "Admin Active"
                                @endphp
                              @endif
  														<td> <span class="label label-success">{{$stage}}</span> </td>
  														<td> <a href="/luckylu/reports/view/{{$posts->id}}"><i class="fa fa-eye"></i></a> </td>
                              <td> <a href="/luckylu/listings/edit/{{$posts->id}}"><i class="fa fa-edit"></i></a> </td>
  														<td>
                                  {{-- {!! Form::open(['action' => ['AdminReportController@listdestroy',$posts->id], 'method'=>'POST']) !!}
                                  {{Form::hidden('_method','delete')}}
                                  {{Form::submit('Delete')}}
                                  {!! Form::close() !!} --}}
                                  <a href="/luckylu/reports/listing/{{$posts->id}}"><i class="fa fa-remove"></i></a>
                                  {{-- <form class="" action="{{route('list.destroy')}}" method="post">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="delete_id" value="{{$posts->id}}">
                                    <input type="submit" class="fa fa-remove" name="delete" >
                                  </form> --}}
                                </td>
  													</tr>
                          @endforeach

												</tbody>
											</table>
										</div>
									</div>
								</div>
								<div class="admin-pag-na">
									<ul class="pagination list-pagenat">
										<li class="disabled"><a href="#!!"><i class="material-icons">chevron_left</i></a> </li>
										<li class="active"><a href="#!">1</a> </li>
										<li class="waves-effect"><a href="#!">2</a> </li>
										<li class="waves-effect"><a href="#!">3</a> </li>
										<li class="waves-effect"><a href="#!">4</a> </li>
										<li class="waves-effect"><a href="#!">5</a> </li>
										<li class="waves-effect"><a href="#!">6</a> </li>
										<li class="waves-effect"><a href="#!">7</a> </li>
										<li class="waves-effect"><a href="#!">8</a> </li>
										<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a> </li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--== BOTTOM FLOAT ICON ==-->
	<section>
		<div class="fixed-action-btn vertical">
			<a class="btn-floating btn-large red pulse"> <i class="large material-icons">mode_edit</i> </a>
			<ul>
				<li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a> </li>
				<li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a> </li>
				<li><a class="btn-floating green"><i class="material-icons">publish</i></a> </li>
				<li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a> </li>
			</ul>
		</div>
	</section>
	<!--SCRIPT FILES-->
	<script src="{{config('app.url')}}/js/jquery.min.js"></script>
	<script src="{{config('app.url')}}/js/bootstrap.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/materialize.min.js" type="text/javascript"></script>
	<script src="{{config('app.url')}}/js/custom.js"></script>
</body>


<!-- Mirrored from rn53themes.net/themes/demo/directory/admin-all-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 11 Dec 2017 18:48:36 GMT -->
</html>

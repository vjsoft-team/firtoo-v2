<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Redirect;

class CityController extends Controller
{
  // public function __construct()
  // {
  //     $this->middleware('auth:admin');
  // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $list = City::all();
      // return 'poi';
      return view('backend.city.list')->with('list', $list);
    }

    public function cityAll()
    {
      $list = City::all();
      return $list;
    }

    // public function cityAllData()
    // {
    //   $data;
    //   $list = City::all();
    //   // return $list;
    //   foreach ($list as $city) {
    //
    //       ];
    //   }
    //   return $data;
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'state' => 'required',
        'city' => 'required',
        'pincode' => 'required'
      ]);
      $add = new City();
      $add->state = $request->state;
      $add->city = $request->city;
      $add->pincode = $request->pincode;
        $add->save();
        return redirect('/luckylu/city');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $list = City::find($id);
     return view('backend.city.edit')->with('key', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'state' => 'required',
        'city' => 'required',
        'pincode' => 'required'
      ]);
      $add = City::find($id);
      $add->state = $request->state;
      $add->city = $request->city;
      $add->pincode = $request->pincode;
        $add->save();
        return redirect('/luckylu/city');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = City::find($id);
      $destroy_info->delete();
      return redirect('/luckylu/city');
    }

    public function selectCity(Request $request)
    {
      session(['city' => $request->city]);
      return Redirect::back();
      // return session()->get('city');
    }
}

<?php

namespace App\Http\Controllers;

use App\ClassifiedReviews;
use Illuminate\Http\Request;
use Auth;

class ClassifiedReviewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $this->validate($request,[
          'review' => 'required',
        ]);
        $add = new ClassifiedReviews();
        $add->classifiedid = $request->classifiedid;
        $add->userid = Auth::user()->id;
        $add->review = $request->review;
        $add->save();
        return redirect("/classifieds/$add->classifiedid");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ClassifiedReviews  $classifiedReviews
     * @return \Illuminate\Http\Response
     */
    public function show(ClassifiedReviews $classifiedReviews)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ClassifiedReviews  $classifiedReviews
     * @return \Illuminate\Http\Response
     */
    public function edit(ClassifiedReviews $classifiedReviews)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ClassifiedReviews  $classifiedReviews
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClassifiedReviews $classifiedReviews)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ClassifiedReviews  $classifiedReviews
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClassifiedReviews $classifiedReviews)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;
use App\User;
use App\Addposting;
use App\ListingsGalley;
use Redirect;

class AdminReportController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth:admin');
  }

  public function lists()
  {
      $listings = Listing::all();
      return view('backend.all_listings')->with('list', $listings);
  }
  public function listshow($id)
  {
      $listings = Listing::find($id);
      return view('backend.list_view')->with('list',$listings);
  }

  public function listdestroy($id)
  {
    // return $request;
    $destroy_info = Listing::find($id);
    // return $destroy_info;
    $destroy_info->delete();
    return redirect('/luckylu/reports/listing');
  }

  public function users()
  {
      $listings = User::all();
      return view('backend.all_users')->with('list',$listings);
  }
  public function ads()
  {
      $ads = Addposting::all();
      return view('backend.all_ads')->with('list',$ads);
  }
  public function editads($id)
  {
    // return $id;
      $ads = Addposting::find($id);
      // return $ads;
      return view('backend.edit_ads')->with('key', $ads);
  }
  public function editlists($id)
  {
    // return $id;
      $lst = Listing::find($id);
      // return $ads;
      return view('backend.edit_lists')->with('key', $lst);
  }
  public function update(Request $request)
  {
      // return $request;
    $edit =  Addposting::find($request->id);
    // $edit->user_id = $request->user_id;
    // $edit->category_id = $request->category_id;
    // $edit->subcategory_id = $request->subcategory_id;

    // $edit->item = $request->item;
    // $edit->price = $request->price;
    // $edit->description = $request->description;
    $edit->status = $request->status;
    // $edit->bought_year = $request->bought_year;

    $edit->save();
    return redirect('/luckylu/reports/classifieds');

  }
  public function updatelists(Request $request)
  {
      // return $request;
    $newListing =  Listing::find($request->id);
    // $newListing->user_id = $request->user_id;
    // $newListing->title = $request->title;
    // $newListing->phone = $request->phone;
    // $newListing->email = $request->email;
    // $newListing->address = $request->address;
    // $newListing->city_id = $request->city_id;
    // $newListing->category_id = $request->category_id;
    // $newListing->opening_days = $request->opening_days;
    // $newListing->open_time = $request->open_time;
    // $newListing->closing_time = $request->closing_time;
    // $newListing->description = $request->description;
    // $newListing->about = $request->about;
    // $newListing->facebook = $request->facebook;
    // $newListing->google = $request->google;
    // $newListing->twitter = $request->twitter;
    // $newListing->google_map = $request->google_map;
    // $newListing->vr_map = $request->vr_map;
    // $newListing->cover = $request->cover;
    // $newListing->gallery = $request->gallery;
    $newListing->status = $request->status;
    $newListing->save();
    return redirect('/luckylu/reports/listing');

  }
  public function adsdestroy($id)
  {
    $destroy_info = Addposting::find($id);
    $destroy_info->delete();
    return redirect('/luckylu/reports/classifieds');
  }
  public function deleteimage($id, $img)
  {
    // die($img);
    $destroy_info = Addposting::find($id);
    if ($img == 'image') {
      $destroy_info->image = '';
    } elseif ($img == 'image2') {
      $destroy_info->image2 = '';
    } elseif ($img == 'image3') {
      $destroy_info->image3 = '';
    }
    $destroy_info->save();
    return redirect("/luckylu/classifieds/edit/$id");
  }
  public function gallerydelete($id)
  {

    $destroy_info = ListingsGalley::find($id);
    $destroy_info->delete();
    // $destroy_info->save();
    return Redirect::back();
    // return redirect("/luckylu/listings/edit/$id");
  }
  public function coverdelete($id)
  {

    $destroy_info = Listing::find($id);
    $destroy_info->cover = '';
    $destroy_info->save();
    return redirect("/luckylu/listings/edit/$id");
  }
}

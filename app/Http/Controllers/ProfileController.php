<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('frontend.profile');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $list = User::find($id);
        return view('frontend.editprofile')->with('key',$list);
    }
    public function passwordedit($id)
    {
        $list = User::find($id);
        return view('frontend.update_password')->with('key',$list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
      $id = $request->id;
      $this->validate($request, [
          'name' => 'required|string|max:255',
          // 'password' => 'required|string|min:6|confirmed',
      ]);
      // if ($request->hasFile('image')) {
      //
      //   $file = $request->image;
      //   $fullPhotoNameWithExt = $file->getClientOriginalName();
      //   $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
      //   $fileExt = $file->getClientOriginalExtension();
      //   $photoToSave = $fileName.'_'.time().'.'. $fileExt;
      //   $path = $file->move('profile', $photoToSave);
      // }
      $add = User::find($id);
      $add->name = $request->name;
      // $add->password = bcrypt($request['password']);
      $add->dob = $request->dob;
      $add->address = $request->address;
      // $add->image = $photoToSave;
      $add->save();
      return redirect('/intel');

    }
    public function updatepassword(Request $request)
    {
      $this->validate($request, [
          'password' => 'required|string|min:6|confirmed',
      ]);

      $add = User::find($request->id);
      $add->password = bcrypt($request['password']);

      $add->save();
      return redirect('/intel');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

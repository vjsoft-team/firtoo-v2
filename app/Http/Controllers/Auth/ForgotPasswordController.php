<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

use Illuminate\Http\Request;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function forgetPasswordView()
    {
      return view('auth/passwords/email');
    }

    public function forgetPasswordOtp(Request $request)
    {
      // dd($request);
      // return $request;
      $message = [];
      $user = User::where('mobile', $request->mobile)->take(1)->get();
      // return $user;
      if (count($user)) {
        $userId = $user[0]['id'];
        $mobile = $user[0]['mobile'];
        $message['status'] = "ok";
        $rand = rand(111111, 999999);
        $otpMessage = $rand . ' is the One-time password (OTP) for your Firtoo Forget Password Verification.';
        $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=gaganam&password=vinny9393&to={$mobile}&from=Firtoo&message=".urlencode($otpMessage);
        $findUser = User::find($userId);
        $findUser->forget_otp = $rand;
        $findUser->save();
        $message['user'] = $findUser;
        file($url);
      } else {
        $message['status'] = "User Not Found";
      }

      // return $message;
      return view('auth/passwords/resetPassword')->with('data', $message);
    }

    public function forgetPasswordUpdate(Request $request)
    {
      $message = [];
      $finduser = User::Where('forget_otp', $request->otp)->first();
      // return $finduser->id;
      if ($finduser->id != '') {
        $userId = $finduser->id;
        $message['status'] = 'ok';
        $updateUser = User::find($userId);
        $updateUser->password = bcrypt($request['password']);
        $updateUser->forget_otp = '';
        $updateUser->save();
        // return $updateUser;
        // code...
      } else {
        $message['status'] = 'Opt Error';
      }

      // return $message;
      return view('auth/passwords/resetPasswordSuccess')->with('data', $message);

    }
}

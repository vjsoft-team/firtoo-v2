<?php

namespace App\Http\Controllers;

use App\Listing;
use App\ListingsGalley;
use Auth;
use Illuminate\Http\Request;

// use Image;

// use ImageOptimizer;

class ListingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
        $list = Listing::all();
        return view('frontend.mylistings')->with('list',$list);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.addlisting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
          'title' => 'required|string|max:255',
          'slug' => 'required|unique:listings|string|max:255',
          'cover' => 'required|mimes:jpeg,jpg,png|max:5000',
          'gallery.*' => 'mimes:jpeg,jpg,png|max:5000',
          'phone' => 'required|max:10',
          'email' => 'required|email',
          'address' => 'required|string|max:255',
          'city' => 'required|string|max:255',
          'category' => 'required',
          'opening_days' => 'required',
          'open_time' => 'required',
          'closing_time' => 'required',
          'description' => 'required',
        ]);
        // return 'llll';


        if ($request->hasFile('cover')) {

          $file = $request->cover;
          // return $file;
          // $img = Image::make($file->getRealPath());
          // $fileExt1 = $file->getClientOriginalExtension();
          // $fullPhotoNameWithExt1 = $file->getClientOriginalName();
          // $fileName1 = pathinfo($fullPhotoNameWithExt1, PATHINFO_FILENAME);
          // $photoToSave1 = "listings/".$fileName1.'_'.time().'.'. $fileExt1;
          // // $url =
          // $filename = $this->compress_image($_FILES["cover"]["tmp_name"], $photoToSave1, 80);
          // $buffer = file_get_contents($photoToSave1);
          $imageFileName = time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/listings/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          $s3 = \Storage::disk('s3');
          $filePath = '/listings/' . $imageFileName;
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave1 = $imageFileNameFull;
        }else {
          $photoToSave1 = ' ';
        }

        $newListing = new Listing();
        $newListing->user_id = Auth::user()->id;
        $newListing->title = $request->title;
        $newListing->slug = $request->slug;
        $newListing->phone = $request->phone;
        $newListing->email = $request->email;
        $newListing->address = $request->address;
        $newListing->city = $request->city;
        $newListing->category_id = $request->category;
        $newListing->subcategory_id = $request->subCategory;
        $newListing->opening_days = $request->opening_days;
        $newListing->open_time = $request->open_time;
        $newListing->closing_time = $request->closing_time;
        $newListing->description = $request->description;
        $newListing->about = $request->about;
        $newListing->facebook = $request->facebook;
        $newListing->google = $request->google;
        $newListing->twitter = $request->twitter;
        $newListing->google_map = $request->google_map;
        $newListing->cover = $photoToSave1;
        // $newListing->gallery = $photoToSave;
        $newListing->status = $request->status;
        $newListing->save();
        if ($request->hasFile('gallery')) {
          $s3 = \Storage::disk('s3');
          foreach ($request->gallery as $photo) {
            // return $request->gallery;
            // return $photo;
            // $filename = $photo->store('gallery');
            $file = $photo;
            $rand = rand(11111111, 99999999);
            $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
            $location = env('AWS_DEFAULT_REGION');
            $bucket = env('AWS_BUCKET');
            $filePath = '/listings_gallery/' . $imageFileName;
            $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
            $s3->put($filePath, file_get_contents($file), 'public');
            $photoToSave1 = $imageFileName;
            // echo $filename;
            // echo "<pre>";
            // return $filename;
            $newGallery = new ListingsGalley;
            $newGallery->listing_id = $newListing->id;
            $newGallery->image = $imageFileNameFull;
            $newGallery->other_city = $request->other_city;
            $newGallery->save();
            // ListingsGalley::create([
            //     'listing_id' => $newListing->id,
            //     'image' => $filename
            // ]);
            // return 'lko';
          }
          // echo public_path();
          // return $newListing->id;
          // $file = $request->gallery;
          // $fullPhotoNameWithExt = $file->getClientOriginalName();
          // $fileName = pathinfo($fullPhotoNameWithExt, PATHINFO_FILENAME);
          // $fileExt = $file->getClientOriginalExtension();
          // $photoToSave = $fileName.'_'.time().'.'. $fileExt;
          // $path = $file->move('listings', $photoToSave);
        }else {
          $photoToSave = ' ';
        }
        return redirect('/success');

    }
    public function success()
    {
      return view('frontend.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function show(Listing $listing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = Listing::find($id);
        return view('frontend.edit_listing')->with('key',$check);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // return $request;
      // $file = $request->cover;
      // return $file->getClientOriginalExtension();
      $this->validate($request,[
        'title' => 'required|string|max:255',
        'phone' => 'required|max:10',
        'email' => 'required|email',
        'address' => 'required|string|max:255',
        'city' => 'required|string|max:255',
        'category' => 'required',
        'opening_days' => 'required',
        'open_time' => 'required',
        'closing_time' => 'required',
        'description' => 'required',
      ]);
      $newListing =  Listing::find($id);
      if ($request->hasFile('gallery')) {
        // return 'pp';
        foreach ($request->gallery as $photo) {
          $file = $photo;
          $rand = rand(11111111, 99999999);
          $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
          $location = env('AWS_DEFAULT_REGION');
          $bucket = env('AWS_BUCKET');
          $filePath = '/listings_gallery/' . $imageFileName;
          $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
          // return $imageFileNameFull;
          $s3 = \Storage::disk('s3');
          $s3->put($filePath, file_get_contents($file), 'public');
          $photoToSave1 = $imageFileNameFull;
          // echo $filename;
          // echo "<pre>";
          // return $filename;
          $newGallery = new ListingsGalley;
          $newGallery->listing_id = $newListing->id;
          $newGallery->image = $photoToSave1;
          $newGallery->save();
      }
    }
      if ($request->hasFile('cover')) {
        // $newListing->cover = '';
        $file = $request->cover;
        // $fullPhotoNameWithExt3 = $file3->getClientOriginalName();
        // $fileName3 = pathinfo($fullPhotoNameWithExt3, PATHINFO_FILENAME);
        // $fileExt3 = $file3->getClientOriginalExtension();
        // $photoToSave2 = $fileName3.'_'.time().'.'. $fileExt3;
        // $path3 = $file3->move('listings', $photoToSave2);
        $rand = rand(11111111, 99999999);
        $imageFileName = $rand.time() . '.' . $file->getClientOriginalExtension();
        $location = env('AWS_DEFAULT_REGION');
        $bucket = env('AWS_BUCKET');
        $filePath = '/listings/' . $imageFileName;
        $imageFileNameFull = "https://s3.$location.amazonaws.com/$bucket$filePath";
        $s3 = \Storage::disk('s3');
        $s3->put($filePath, file_get_contents($file), 'public');
        $photoToSave2 = $imageFileNameFull;
        // return $photoToSave2;
        $newListing->cover = $photoToSave2;
        $newListing->user_id = Auth::user()->id;
        $newListing->title = $request->title;
        $newListing->phone = $request->phone;
        $newListing->email = $request->email;
        $newListing->address = $request->address;
        $newListing->city = $request->city;
        $newListing->category_id = $request->category;
        $newListing->subcategory_id = $request->subcategory;
        $newListing->opening_days = $request->opening_days;
        $newListing->open_time = $request->open_time;
        $newListing->closing_time = $request->closing_time;
        $newListing->description = $request->description;
        $newListing->facebook = $request->facebook;
        $newListing->google = $request->google;
        $newListing->twitter = $request->twitter;
        $newListing->google_map = $request->google_map;
        $newListing->cover = $photoToSave2;
        $newListing->status = 1;
        $newListing->save();
      } else {
        $newListing->user_id = Auth::user()->id;
        $newListing->title = $request->title;
        $newListing->phone = $request->phone;
        $newListing->email = $request->email;
        $newListing->address = $request->address;
        $newListing->city = $request->city;
        $newListing->category_id = $request->category;
        $newListing->subcategory_id = $request->subcategory;
        $newListing->opening_days = $request->opening_days;
        $newListing->open_time = $request->open_time;
        $newListing->closing_time = $request->closing_time;
        $newListing->description = $request->description;
        $newListing->facebook = $request->facebook;
        $newListing->google = $request->google;
        $newListing->twitter = $request->twitter;
        $newListing->google_map = $request->google_map;
        // $newListing->cover = $photoToSave2;
        $newListing->status = 1;
        $newListing->save();
      }

      return redirect('/success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Listing  $listing
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $destroy_info = Listing::find($id);
      $destroy_info->delete();
      return redirect('/listing');
    }

    function compress_image($source_url, $destination_url, $quality) {

		$info = getimagesize($source_url);

    		if ($info['mime'] == 'image/jpeg')
        			$image = imagecreatefromjpeg($source_url);

    		elseif ($info['mime'] == 'image/gif')
        			$image = imagecreatefromgif($source_url);

   		elseif ($info['mime'] == 'image/png')
        			$image = imagecreatefrompng($source_url);

    		imagejpeg($image, $destination_url, $quality);
		return $destination_url;
	}
}

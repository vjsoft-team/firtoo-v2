<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class MobileVerificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($mobile)
    {
        // return $mobile;
        $oldRand = User::where('mobile', $mobile)->first();
        if ($oldRand->status == 1) {
          return redirect('/login');
        }
        $tableRand = $oldRand->otp;
        if ($oldRand->messageStatus == 0 && $oldRand->status == 0) {
          $message = $tableRand . ' is the One-time password (OTP) for your Firtoo Registration. This is usable once & valid for 3 mins from the time of request. DO NOT SHARE WITH ANYONE';
          $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=gaganam&password=vinny9393&to={$mobile}&from=Firtoo&message=".urlencode($message);
          file($url);
          // $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=gaganam&password=vinny9393&to={$mobile}&from=FIRTOO&message={$tableRand}";
          // file_get_contents($url);
        } else {
          $rand = rand(111111, 999999);
        }
        $oldRand->messageStatus = 1;
        $oldRand->save();

        // return $oldRand->messageStatus;

        // $newRand = $oldRand->otp;
        return view('frontend.mobileverify')->with('mobile', $mobile);
    }

    public function verifymobileemail($email)
    {
        // return $mobile;
        $oldRand = User::where('email', $email)->first();
        return redirect("verifymobile/$oldRand->mobile") ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
      // return $request;
      $rand = rand(111111, 999999);
      $mobile = $request['mobile'];
      $oldRand = User::where('mobile', $mobile)->first();
      $oldRand->otp = $rand;
      $oldRand->save();
      $tableRand = $oldRand->otp;
      $message = $rand . ' is the One-time password (OTP) for your Firtoo Registration. This is usable once & valid for 3 mins from the time of request. DO NOT SHARE WITH ANYONE';
      $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=gaganam&password=vinny9393&to={$mobile}&from=Firtoo&message=".urlencode($message);
      file($url);
      // $ch = curl_init();
      // $headers = array(
      //   'Accept: application/json',
      //   'Content-Type: application/json',
      // );
      // curl_setopt($ch, CURLOPT_URL, "http://onlinebulksmslogin.com/spanelv2/api.php?username=gaganam&password=vinny9393&to={$mobile}&from=Firtoo&message=is");
      // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      // curl_setopt($ch, CURLOPT_HEADER, 0);

      //$body = '{}';
      //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      //curl_setopt($ch, CURLOPT_POSTFIELDS,$body);
      // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      return redirect("verifymobile/$mobile");

    }

    public function store(Request $request)
    {
      // return $request;
        $mobile = $request['mobile'];
        $otp = $request['otp'];
        $user = User::where('mobile', $mobile)->where('otp', $otp)->get();
        // return $user;
        if ($user->count()) {
          $user->first()->status = 1;
          $user->first()->save();
          $userEmail = $user->first()->email;
          $message = "Congrats you have registered successfully, Please Login!";
          $data = [
            'message' => $message,
            'useremail' => $userEmail
          ];

          return view('auth.login')->with('data', $data);
          // return $user;
        } else {
          $message = "Wrong Otp";
          return redirect()->back()->with('status', "Wrong Otp");
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}

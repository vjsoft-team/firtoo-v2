<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Listing;
use App\Category;
use DB;

class SearchController extends Controller
{
    public function listingForward(Request $request)
    {
      if ($request->city == '') {
        return redirect("/search/listing/tirupati/$request->category");
      } else {
        return redirect("/search/listing/$request->city/$request->category");
      }
    }

    public function listing($city, $listing)
    {
      session(['city' => $city]);
      $category = Category::where('name', $listing)->get()->take(1);
      $category = DB::SELECT("SELECT id FROM categories WHERE name = '$listing'");
      $ids = [];
      foreach ($category as $cat) {
        $id = DB::SELECT("SELECT id FROM listings WHERE category_id = '$cat->id'");
        if (count($id)) {
          // code...
          array_push($ids, ...$id);
        }
      }
      $manual = DB::SELECT("SELECT id FROM listings WHERE title LIKE '%$listing%'");
      // return $manual;
      if (count($manual)) {
        // code...
        array_push($ids, ...$manual);
      }
      // return $ids;

$idf = implode(', ', array_map(function ($entry) {

return $entry->id;

}, $ids));
// return $idf;
      if ($idf != '') {
        // return $idf;
        // $categoryId = $category->first()->id;
        $data = [
          'ids' => $idf,
          'city' => $city
        ];
      } else {
        $data = [
          'message' => 'no'
        ];
      }
      // return $categoryId;
      return view('frontend.searchListings')->with('data', $data);
      // return $listing;
      // return redirect("/serach/listing/$request->city/$request->category");
    }

    public function listingForwardClassifieds(Request $request)
    {
      if ($request->city == '') {
        return redirect("/search/classifieds/tirupati/$request->category");
      } else {
        return redirect("/search/classifieds/$request->city/$request->category");
      }
    }

    public function classifieds($city, $listing)
    {
      session(['city' => $city]);
      // return session()->get('city');
      $category = Category::where('name', $listing)->get()->take(1);
      $categoryId = $category->first()->id;
      // return $categoryId;
      $data = [
        'categoryId' => $categoryId,
        'city' => $city
      ];
      return view('frontend.searchListingsClassifieds')->with('data', $data);
      // return $listing;
      // return redirect("/serach/listing/$request->city/$request->category");
    }
}

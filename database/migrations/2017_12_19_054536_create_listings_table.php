<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('phone');
            $table->string('email');
            $table->text('address');
            $table->text('about')->nullable();
            $table->string('city_id')->nullable();
            $table->string('city');
            $table->string('category_id');
            $table->string('subcategory_id')->nullable();
            $table->string('opening_days');
            $table->string('open_time');
            $table->string('closing_time');
            $table->text('description');
            $table->text('facebook')->nullable();
            $table->text('google')->nullable();
            $table->text('twitter')->nullable();
            $table->text('google_map')->nullable();
            $table->text('vr_map')->nullable();
            $table->string('cover')->nullable();
            $table->string('gallery')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('listings');
    }
}
